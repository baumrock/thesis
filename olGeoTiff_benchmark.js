// olGeoTiff class

/**
 * base class for openlayers geotiff support
 * @param {*} layer 
 */

 var delayTimer;

function olGeoTiff(layer) {
  // layer of the OL map that holds the tiff tiles
  this.layer = layer;
  this.tilesStarted = 0;
  this.tilesDone = 0;
  this.testActive = false;

  // options object for plotty plot
  this.plotOptions = {
    domain: [-1, 1],
    width: 256,
    height: 256,
    noDataValue: false,
    palette: null,

    /**
     * data array
     * @param rasters parsed geotiff multiband data
     * @returns array of float values calculated by dataFunction()
     */
    data: function(rasters) {
      var data = [];

      // loop every single datapoint
      for(var i = 0; i<rasters[0].length; i++) {
        var bands = [];

        // add all other available bands to this array
        for(var b = 0; b<rasters.length; b++) bands.push(rasters[b][i]);

        // calculate single float value for this multiband array
        // push float value to the data array and continue with next datapoint
        data.push( this.dataFunction(bands) ); 
      }

      // return the data array
      return data;
    },

    /**
     * data function
     * @param b array single arrayitem of parsed geotiff multiband data
     * @return float calculated value (by default returns the first band)
     */
    dataFunction: function(b) {
      return b[1];
    },
  };

  // object that holds all rastered tiffs identified by their url
  this.urlToTiff = {};

  // plotty instance for this layer
  this.plot = new plotty.plot({});

  /**
   * create benchmark object
   */
    this.benchy = {
      client: {
        interactions: [],
      },
      server: {
        interactions: [],
      },
      // return last interaction of client or server
      lastInteraction: function(where) {
        var i = this[where].interactions;
        return i[i.length-1];
      },
      total: function(where, what) {
        var interactions = this[where].interactions;
        var total = 0;
        for(i=0; i<interactions.length; i++) {
          if(typeof interactions[i][what] == 'function') total += interactions[i][what]();
          else total += interactions[i][what]*1;
        }
        return total;
      }
    }
    startInteraction('Initial Map load', this.benchy);

  // this sets the custom tile load function on init of this class
  this.layer.getSource().setTileLoadFunction(this.tileLoadFunction.bind(this));
}

/**
 * fetch tiff and set callbacks
 * @param {*} url url of the geotiff file
 * @param {*} listener callback on ajax success
 * @param {*} errorListener callback on ajax error
 */
olGeoTiff.prototype.fetchTiff = function(url, listener, errorListener) {
  benchyAdd('ptcp', 0, 'client'); // make sure ptcp is set
  benchyAdd('ptcr', 0, 'client'); // make sure ptcr is set

  var urlToTiff = this.urlToTiff;
  if (urlToTiff[url]) {
    // in this case the tiff is already received and parsed
    if (urlToTiff[url].rasters) {
      listener(urlToTiff[url]);
    }
    else if (urlToTiff[url].error) {
      errorListener(urlToTiff[url].error);
    }
    // in this case the tiff was already requested
    else {
      urlToTiff[url].listeners.push(listener);
      urlToTiff[url].errorListeners.push(errorListener);
    }
  }
  // in this case the tiff was not yet requested
  else {
    urlToTiff[url] = {
      rasters: null,
      error: null,
      listeners: [listener],
      errorListeners: [errorListener],
    };

    // send new request
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    var that = this;
  
    // setup the async function that is executed AFTER the tile was loaded
    xhr.onloadend = function(e) {
      if (xhr.status == 200) {
        // benchmarking
        var tileSize = xhr.getResponseHeader("Content-Length")/1024;
        benchyAdd('dt', tileSize, 'client');
        
        var now = performance.now();
        if(!urlToTiff[url].loadEnd || urlToTiff[url].loadEnd < now) urlToTiff[url].loadEnd = now;

        // save rasters of parsed tiff
        var beforeparse = performance.now();
        var parsed = GeoTIFF.parse(this.response);
        urlToTiff[url].rasters = parsed.getImage().readRasters();
        benchyAdd('ptcp', performance.now()-beforeparse, 'client');

        var listeners = urlToTiff[url].listeners;
        for (var i = 0; i < listeners.length; ++i) listeners[i](urlToTiff[url]);
        urlToTiff[url].listeners = [];

        benchySetHigh('rtEnd', performance.now());
        benchyAdd('not', 1, 'client');
      }
      else {
        urlToTiff[url].error = e;
        var errorListeners = urlToTiff[url].errorListeners;
        for (var i = 0; i < errorListeners.length; ++i) errorListeners[i](e);
        urlToTiff[url].errorListeners = [];
      }

      if(olgt.tilesDone >= olgt.tilesStarted) $(document).trigger('interactiondone');
    }
  
    // set start timestamp for benchmark
    benchySetLow('rtStart', performance.now());    

    // send ajax request
    xhr.send();
  }
}

/**
 * custom tile load function
 * @param {*} imageTile 
 * @param {*} src 
 */
olGeoTiff.prototype.tileLoadFunction = function(imageTile, src) {
  this.tilesStarted++;

  // replace the imageTile with a canvas
  var imageCanvas = document.createElement('canvas');
  imageCanvas.naturalWidth = this.plotOptions.width;
  imageCanvas.naturalHeight = this.plotOptions.height;
  imageCanvas.width = this.plotOptions.width;
  imageCanvas.height = this.plotOptions.height;
  imageTile.unlistenImage_();
  imageTile.image_ = imageCanvas;

  imageTile.imageListenerKeys_ = [
    ol.events.listenOnce(imageTile.image_, ol.events.EventType.ERROR,
      imageTile.handleImageError_, imageTile),
    ol.events.listenOnce(imageTile.image_, ol.events.EventType.LOAD,
      imageTile.handleImageLoad_, imageTile)
  ];

  // fetch data of this tile
  this.fetchTiff(
    // url of tile
    src,
    
    // callback function that executes when the tiff is parsed and ready
    function(urlToTiff) {
      // get plotty instance
      var plot = this.plot;

      // set plotty settings
      plot.setDomain(this.plotOptions.domain);
      plot.setData(
        this.plotOptions.data(urlToTiff.rasters),
        this.plotOptions.width,
        this.plotOptions.height
      );
      if(this.plotOptions.palette)
        plot.setColorScale(this.plotOptions.palette);
      if(this.plotOptions.noDataValue !== false)
        plot.setNoDataValue(this.plotOptions.noDataValue);

      // render plot and trigger load event
      var beforeplot = performance.now();
      plot.render();
      imageCanvas.getContext('2d').drawImage(plot.getCanvas(), 0, 0);
      imageCanvas.dispatchEvent(new Event('load'));

      benchyAdd('ptcr', performance.now()-beforeplot, 'client');

      // every rendered tile would have had to be requested from server
      // so we add the time configured in the settings for each plotted tile
      // console.log('add dt val');
      // console.log('tilesStarted = ' + olgt.tilesStarted);
      // console.log('tilesDone = ' + olgt.tilesDone);
      benchyAdd('dt', $('#settings_tilesize').val(), 'server');
      benchyAdd('not', 1, 'server');
      
      // trigger interactiondone event when interaction was only on the client side
      // delay of 100ms to call it only once
      function fireEvent(text) {
        clearTimeout(delayTimer);
        delayTimer = setTimeout(function() {

          // calculate rt for server
          var last = olgt.benchy.lastInteraction('server');

          last.rts = last.not*$('#settings_pts').val() + (last.dt/getNS())*1000;
          last.pts = 1*$('#settings_pts').val();

          $(document).trigger('interactiondone');
        }, 100); // makes sure this is called not more than every 100ms
      }
      fireEvent();
    }.bind(this),

    // callback function in case of AJAX error
    function(error) {
      // trigger error event
      imageCanvas.dispatchEvent(new Event('error'));
    }
  );
};

/**
 * redraw the given layer
 */
olGeoTiff.prototype.redraw = function() {
  this.layer.getSource().refresh();

  $(document).trigger('interactiondone');
  olgt.tilesStarted = 0;
  olgt.tilesDone = 0;
}


// ############### benchmarking ###############

/**
 * kpi object for benchy
 */
var Interaction = function() {
  return {
    title: null,
    noi: 0, 
    not: 0, // num of tiles requested
    rt: function() {
      if(this.rts) return this.rts;
      if(!this.rtEnd || !this.rtStart) return this.ptc();
      return this.rtEnd - this.rtStart + this.ptc();
    },
    rts: null, // request time for server
    pts: null,
    dt: null,
    ptcp: null,
    ptcr: null,
    ptc: function() { return this.ptcp + this.ptcr; },
    rtStart: null, // timestamp when requests started
    rtEnd: null // timestamp when requests finished
  };
}

/**
 * start new interaction
 */
var startInteraction = function(title, benchy) {
  // push interaction to both client and server
  for(where in {server:null, client:null}) {
    var interaction = new Interaction();
    interaction.title = title;
    interaction.noi++;
  
    var line = 0;
    if(benchy) {
      benchy[where].interactions.push(interaction);
      line = benchy['client'].interactions.length;
    }
    else {
      olgt.benchy[where].interactions.push(interaction);
      line = olgt.benchy['client'].interactions.length;
    }
  }

  $('#log').html(line + ': ' + title+"\n"+$('#log').html());
}

/**
 * add count to benchy value
 */
var benchyAdd = function(prop, val, where) {
  if(where == undefined) both = {server:null, client:null};
  else if(where == 'server') both = {server:null};
  else if(where == 'client') both = {client:null};

  var last;
  for(where in both) {
    last = olgt.benchy.lastInteraction(where);  
    last[prop] = last[prop]*1 + val*1;
  }
}

/**
 * set lowest peak value
 */
var benchySetLow = function(prop, val) {
  var last;
  for(where in {server:null, client:null}) {
    last = olgt.benchy.lastInteraction(where);
    if(last[prop] === null || last[prop] > val) last[prop] = val;
  }
}

/**
 * set highest peak value
 */
var benchySetHigh = function(prop, val) {
  var last;
  for(where in {server:null, client:null}) {
    last = olgt.benchy.lastInteraction(where);
    if(last[prop] === null || last[prop] < val) last[prop] = val;
  }
}

/**
 * return estimate network speed
 */
var getNS = function() {
  if(!olgt.benchy.total('client', 'rt')) return 0;
  return olgt.benchy.total('client', 'dt')/(olgt.benchy.total('client', 'rt')/1000);
}