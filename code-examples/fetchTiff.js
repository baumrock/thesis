olGeoTiff.prototype.fetchTiff = function(url, listener, errorListener) {
  var urlToTiff = this.urlToTiff;
  if (urlToTiff[url]) {
    // in this case the tiff is already received and parsed
    if (urlToTiff[url].rasters) {
      listener(urlToTiff[url]);
    }
    else if (urlToTiff[url].error) {
      errorListener(urlToTiff[url].error);
    }
    // in this case the tiff was already requested
    else {
      urlToTiff[url].listeners.push(listener);
      urlToTiff[url].errorListeners.push(errorListener);
    }
  }
  // in this case the tiff was not yet requested
  else {
    urlToTiff[url] = {
      rasters: null,
      error: null,
      listeners: [listener],
      errorListeners: [errorListener]
    };

    // send new request
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    var that = this;
  
    // setup the async function that is executed AFTER the tile was loaded
    xhr.onloadend = function(e) {
      if (xhr.status == 200) {
        // save rasters of parsed tiff
        var parsed = GeoTIFF.parse(this.response);
        urlToTiff[url].rasters = parsed.getImage().readRasters();
        var listeners = urlToTiff[url].listeners;
        for (var i = 0; i < listeners.length; ++i) listeners[i](urlToTiff[url]);
        urlToTiff[url].listeners = [];
      }
      else {
        urlToTiff[url].error = e;
        var errorListeners = urlToTiff[url].errorListeners;
        for (var i = 0; i < errorListeners.length; ++i) errorListeners[i](e);
        urlToTiff[url].errorListeners = [];
      }
    }
  
    // send ajax request
    xhr.send();
  }
}