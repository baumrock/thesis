olGeoTiff.prototype.tileLoadFunction = function(imageTile, src) {
  // replace the imageTile with a canvas
  
  // fetch data of this tile
  this.fetchTiff(
    src, // url of tile
    
    // callback function that executes when the tiff is parsed and ready
    function(urlToTiff) {
      // get plotty instance
      // set plotty settings
      // render plot and trigger load event
    }.bind(this),

    // callback function in case of AJAX error
    function(error) {
      imageCanvas.dispatchEvent(new Event('error')); // trigger error event
    }
  );
};

